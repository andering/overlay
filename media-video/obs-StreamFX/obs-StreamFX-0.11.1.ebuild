# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="OBS plugin to integrate StreamFX"
HOMEPAGE="https://github.com/Xaymar/obs-StreamFX"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	media-video/obs-studio
"
RDEPEND="${DEPEND}"

src_prepare() {
	default

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		"-DLIBOBS_INCLUDE_DIR=/usr/include/obs/UI"
	)

	cmake_src_configure
}
