# Copyright 2017 Hacking Networked Solutions
# Distributed under the terms of the GNU General Public License v3
# $Header: $

EAPI="7"

PYTHON_COMPAT=( python3_7 python3_8 python3_9 )

RDEPEND="${PYTHON_DEPS}"
DEPEND="${RDEPEND}"

DESCRIPTION="Google Cloud SDK"
SLOT="0"
SRC_URI="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/${P}-linux-x86_64.tar.gz"

LICENSE="https://cloud.google.com/terms/"
SLOT="0"
KEYWORDS="~amd64"
S="${WORKDIR}/google-cloud-sdk"

src_unpack() {
	if [ "${A}" != "" ]; then
		unpack ${A}
	fi
}

src_install() {
	dodir ${ROOT}/usr/share/google-cloud-sdk
	cp -R "${S}/" "${D}/usr/share/" || die "Install failed!"
	dosym "${ROOT}/usr/share/google-cloud-sdk/bin/gcloud" "/usr/bin/gcloud"
}
