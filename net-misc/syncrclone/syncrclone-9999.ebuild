# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..10} )

inherit distutils-r1 git-r3
DISTUTILS_USE_SETUPTOOLS=rdepend

DESCRIPTION="PyWayland provides a wrapper to the libwayland library using the CFFI library to provide access to the Wayland library calls and written in pure Python."
HOMEPAGE="https://github.com/flacjacket/pywayland"
EGIT_REPO_URI="https://github.com/Jwink3101/syncrclone.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="test"

RDEPEND=""
DEPEND="${RDEPEND}
    test? (
        dev-python/pytest[${PYTHON_USEDEP}]
    )"

python_test() {
    py.test -v tests || die "tests fail with ${EPYTHON}"
}