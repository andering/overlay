# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils gnome2-utils

# get the major version from PV
# MV=${PV:0:1}
# MY_PV=${PV#*_beta}

DESCRIPTION="Meet a new Git Client, from the makers of Sublime Text"
HOMEPAGE="https://www.sublimemerge.com/"
SRC_URI="https://download.sublimetext.com/sublime_merge_build_${PV}_x64.tar.xz"
S=${WORKDIR}/sublime_merge

LICENSE="Sublime"
SLOT="0"
KEYWORDS="~amd64"
IUSE="dbus"
RESTRICT="bindist mirror strip"

RDEPEND="
	dev-libs/glib:2
	x11-libs/gtk+:3
	x11-libs/libX11
	dbus? ( sys-apps/dbus )"

src_install() {
	insinto /opt/sublime_merge
	exeinto /opt/sublime_merge

	doins -r Packages Icon
	doexe sublime_merge

	dosym /opt/sublime_merge/sublime_merge /usr/bin/sublime_merge

#	local size
#	for size in 32 48 128 256; do
#		dosym ../../../../../../opt/${PN}${PV}/Icon/${size}x${size}/sublime-merge.png \
#			/usr/share/icons/hicolor/${size}x${size}/apps/sublime-merge.png
#	done

#	make_desktop_entry "sublime-merge" "Sublime Merge ${PV}" "sublime-merge" \
#		"TextEditor;IDE;Development" "StartupNotify=true"

	# needed to get WM_CLASS lookup right
#	mv "${ED%/}"/usr/share/applications/subl{-sublime-merge,}.desktop || die
}

pkg_postrm() {
	gnome2_icon_cache_update
}

pkg_postinst() {
	gnome2_icon_cache_update
}
