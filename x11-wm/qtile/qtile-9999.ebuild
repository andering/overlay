# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DISTUTILS_USE_SETUPTOOLS=rdepend

PYTHON_COMPAT=( python3_{7,8,9,10} )

inherit distutils-r1 virtualx

if [[ ${PV} == 9999* ]] ; then
	EGIT_REPO_URI="https://github.com/andering/qtile.git"
	inherit git-r3
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="A full-featured, hackable tiling window manager written in Python"
HOMEPAGE="http://qtile.org/"
LICENSE="MIT"
SLOT="0"
IUSE="wayland X test"

RDEPEND="
	x11-libs/cairo[X,xcb(+)]
	x11-libs/pango
	media-sound/pulseaudio
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/setuptools_scm[${PYTHON_USEDEP}]
	dev-python/dbus_next[${PYTHON_USEDEP}]
	dev-python/cairocffi[${PYTHON_USEDEP}]
	dev-python/cffi[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	wayland? (
		dev-python/pywlroots[${PYTHON_USEDEP}]
		dev-python/pywayland[${PYTHON_USEDEP}]
		dev-python/xkbcommon
	)
	X? (
		x11-base/xorg-server[xephyr]
		dev-python/xcffib[${PYTHON_USEDEP}]
	)
"

BDEPEND="
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
		dev-python/xvfbwrapper[${PYTHON_USEDEP}]
		x11-base/xorg-server[xephyr]
		x11-apps/xeyes
		x11-apps/xcalc
		x11-apps/xclock
	)
"

RESTRICT="test"

python_test() {
	# force usage of built module
	rm -rf "${S}"/libqtile || die
	PYTHONPATH="${BUILD_DIR}/lib" py.test -v "${S}"/test || die "tests failed under ${EPYTHON}"
}

python_install_all() {
	local DOCS=( CHANGELOG README.rst )
	distutils-r1_python_install_all

	insinto /usr/share/xsessions
	doins resources/qtile.desktop

	exeinto /etc/X11/Sessions
	newexe "${FILESDIR}"/${PN}-session ${PN}
}



